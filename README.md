# Project to Test Badge generation in CI

[![pipeline status](https://gitlab.com/lorenzocalamandrei/badge-artifact/badges/master/pipeline.svg)](https://gitlab.com/lorenzocalamandrei/badge-artifact/-/commits/master)
[![commits](http://gitlab.com/lorenzocalamandrei/badge-artifact/-/jobs/artifacts/master/raw/commits.svg?job=Generate%20Commit%20Badge)](https://gitlab.com/lorenzocalamandrei/badge-artifact/-/commits/master)
[![tag](http://gitlab.com/lorenzocalamandrei/badge-artifact/-/jobs/artifacts/master/raw/tag.svg?job=Generate%20Tag%20Badge)](https://gitlab.com/lorenzocalamandrei/badge-artifact/-/tags)
